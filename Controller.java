package com.example.demo;

import java.math.BigInteger;
import java.net.URL;

import javax.xml.namespace.QName;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dataaccess.webservicesserver.NumberConversion;
import com.dataaccess.webservicesserver.NumberConversionSoapType;

@RestController
@RequestMapping("/api")
public class Controller {

	 private static final QName SERVICE_NAME = new QName("http://www.dataaccess.com/webservicesserver/", "NumberConversion");

	
	@GetMapping("/metodos")
	public ResponseEntity consumirSoap() {
		URL wsdlURL = NumberConversion.WSDL_LOCATION;
		 
		NumberConversion ss = new NumberConversion(wsdlURL, SERVICE_NAME);
	    NumberConversionSoapType port = ss.getNumberConversionSoap();
	    String reponse = port.numberToWords(new BigInteger("1"));
		return new ResponseEntity(HttpStatus.OK).ok(reponse);
	}
	
	
}
